<?php
namespace Romsonapp\Api\Classes;

use Illuminate\Http\Request;
use Romsonapp\Api\CqeApi;

class GameRequest
{
    private $api;
    public function __construct(CqeApi $cqeApi)
    {
        $this->api = $cqeApi;
    }

    public function getGamesList()
    {
        return $this->api->request('/games');
    }

    public function getGame($game_id)
    {
        return $this->api->request('/game', ['game_id' => $game_id]);
    }

    public function bid($game_id, $team_id)
    {
        return $this->api->request('/bid', ['game_id' => $game_id, 'team_id' => $team_id]);
    }

    public function unbid($game_id, $team_id)
    {
        return $this->api->request('/unbid', ['game_id' => $game_id, 'team_id' => $team_id]);
    }

    public function createGame(Request $request)
    {
        if ($this->api->request('/createGame', $request->all()))
            return true;

        return false;
    }

    public function updateGame($game_id, $request)
    {
        $content = array_merge($request->all(), ['game_id' => $game_id]);
        if ($this->api->request('/updateGame', $content))
            return true;

        return false;
    }

    public function deleteGame($game_id)
    {
        $content = array_merge(['game_id' => $game_id]);
        if ($this->api->request('/deleteGame', $content))
            return true;
        return false;
    }
}