<?php
namespace Romsonapp\Api;

use Guzzle\Http\Client;
use Illuminate\Http\Request;

class CqeApi
{
    private static $_instance;
    private $client;
    private $response;
    public $uri = 'http://localhost:8000';
    private $config = [
        'PUBLIC_KEY' => '79a72570aa2f404062fac48e296d9c6ce8cd4f07',
        'SECRET_KEY' => 'f61a091968af742e30975b8edc1084f6'
    ];

    private function __construct()
    {
        $this->client = new Client($this->uri, $this->config);
    }

    public function request($api, $content = [])
    {
        $param = array_merge($this->config, $content, $_SERVER);
        $request = $this->client->post($api, [], $param);

        try {
            $response = $request->send();
            return json_decode($response->getBody(true));
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function run($api, $params = [])
    {
        if (!self::$_instance)
            self::$_instance = new CqeApi();

        list($class, $method) = explode('.', $api);
        $class = '\Romsonapp\Api\Classes\\' . ucfirst($class) . 'Request';
        $reflection = new \ReflectionMethod($class, $method);

        $class = new $class(self::$_instance);

        if (!is_array($params)) {
            return call_user_func([$class, $method], $params);
        }

        $arguments = [];
        foreach ($reflection->getParameters() as $reflectionParameter) {
            $arguments[] = $params[$reflectionParameter->getName()];
        }

        return call_user_func_array([$class, $method], $arguments);
    }
}